# This script gets urls.

import json
import urllib2
import sys
import urlparse
import httplib
import re

# Recursively follow redirects until there isn't a location header
def resolve_http_redirect(url, depth=0):
    if depth > 10:
        raise Exception("Redirected "+depth+" times, giving up.")
    o = urlparse.urlparse(url,allow_fragments=True)
    conn = httplib.HTTPConnection(o.netloc)
    path = o.path
    if o.query:
        path +='?'+o.query
    conn.request("HEAD", path)
    res = conn.getresponse()
    headers = dict(res.getheaders())
    if headers.has_key('location') and headers['location'] != url:
        return resolve_http_redirect(headers['location'], depth+1)
    else:
        return url

def usage():
    print "python dmoz.py FILE\n"
    sys.exit(1)

def main():
    if len(sys.argv) != 2:
        usage()

    all_urls = set()

    url_re = re.compile(r'"(http://[^"]+)"', re.I)

    # load file into memory and loop over it.
    f = open(sys.argv[1])
    content = ''.join(f.readlines())
    for u in url_re.finditer(content):
        url = u.group(1)
        url = resolve_http_redirect(url)
        if url not in all_urls:
            print url
            all_urls.add(url)

if __name__ == "__main__":
    main()

