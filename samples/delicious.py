# This script gets urls.

import json
import urllib2
import sys

def load_delicious(tags=''):
    delicious = 'http://feeds.delicious.com/v2/json%s?count=100'

    # get the json
    sys.stdout.write("Loading %s..." % (delicious % tags))

    try:
        p = urllib2.urlopen(delicious % tags)
        urls = json.loads(''.join(p.readlines()))
    except urllib2.HTTPError:
        sys.stdout.write('nadda\n')
        return None
    except:
        sys.stdout.write('fml\n')
        return None

    sys.stdout.write('\n')

    return urls

def main():
    all_urls = []

    # get the first 100 URLS
    first_100 = load_delicious()

    for url in first_100:
        # add the current url
        all_urls.append(url['u'])

        # get other urls for the tag and add them
        for tag in url['t']:
            tag_urls = load_delicious('/' + tag)
            if tag_urls:
                for tag_url in tag_urls:
                    try:
                        all_urls.append(tag_url['u'])
                    except KeyError:
                        pass

    # Now we have a shit ton of urls :)
    # ...hm what to do with these urls?
    # I will write them to a file.
    f = open('delicious_urls', 'w')
    for url in all_urls:
        f.write('%s\n' % url)
    f.close()

if __name__ == "__main__":
    main()

