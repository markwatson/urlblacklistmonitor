CREATE TABLE  `url_blacklist`.`rate` (
`id` BIGINT NOT NULL ,
`url` VARCHAR( 255 ) NOT NULL ,
`source` VARCHAR( 255 ) NOT NULL ,
`when` DATETIME NOT NULL ,
`mywot` DATETIME NULL ,
`norton` DATETIME NULL ,
`safeweb` DATETIME NULL ,
PRIMARY KEY (  `id` ) ,
UNIQUE (
`url`
)
) ENGINE = INNODB;


ALTER TABLE `url_blacklist`.`rate` MODIFY COLUMN `id` BIGINT(20)  NOT NULL AUTO_INCREMENT;

ALTER TABLE `url_blacklist`.`rate` ADD COLUMN `mywot_status` VARCHAR(255)  DEFAULT 'unknown' AFTER `safeweb`,
 ADD COLUMN `norton_status` VARCHAR(255)  NOT NULL DEFAULT 'unknown' AFTER `mywot_status`,
 ADD COLUMN `safeweb_status` VARCHAR(255)  NOT NULL DEFAULT 'unknown' AFTER `norton_status`;
