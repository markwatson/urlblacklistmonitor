import sys, os
import subprocess
import datetime
import MySQLdb
import MySQLdb.cursors

def run(arguments):
    return subprocess.Popen(arguments,
        stdout=subprocess.PIPE).communicate()[0]

def from_ts(ts):
    return datetime.datetime.strptime(ts, '%Y%m%d%H%M%S')

def to_ts(dt):
    return dt.strftime('%Y%m%d%H%M%S')

def iter_all_urls(sources):
    """
    Iterate through the urls in the feeds and yield the tuple:
    (url, source, start).
    """
    for source in sources:
        output = run(['python', 'rss_downloader.py', source])
        for line in output.split('\n'):
            parts = line.split(',')
            if len(parts) == 10:
                ltype, version, malicious_url, cat, info_url, source_date,\
                    date_found, ip, md5, desc = parts
                if ltype == 'URL' and version == '1.0':
                    date = from_ts(date_found)
                    # try:
                    #     date = from_ts(source_date)
                    # except ValueError:
                    #     date = from_ts(date_found)
                    yield (malicious_url, source, date)

def try_add(db, urls):
    q ="INSERT IGNORE INTO `rate` (`url`, `source`, `start`) VALUES (%s, %s, %s)"
    cursor = db.cursor()
    for u in urls:
        cursor.execute(q, u)

        db.commit()

def needed_urls(db):
    # get the data
    q = """
    SELECT `id`, `url`, `source`, `start`, `mywot`, `norton`, `safeweb`,
           `mywot_status`, `norton_status`, `safeweb_status`
    FROM rate
    WHERE (`mywot_status` = 'unknown') OR
          (`norton_status` = 'unknown') OR
          (`safeweb_status` = 'unknown')
    """
    cursor = db.cursor()
    cursor.execute(q)
    return cursor.fetchall()

def fmt_url(url):
    if url.startswith("http"):
        return url
    else:
        return "http://" + url

def main():
    print "Starting at %s..." % datetime.datetime.now()

    # get new urls
    urls = iter_all_urls([
        'malc0de.com',
        'hosts-file.net',
        'malwaredomainlist.com',
        'malwareblacklist.com',
    ])

    # connect to the database
    db = MySQLdb.connect(user='url_blacklist',passwd='W7AbrUwR',db='url_blacklist',
        cursorclass=MySQLdb.cursors.DictCursor)

    # deposit the urls into the database
    print "Adding new urls..."
    try_add(db, list(urls))

    print "Checking all urls..."
    # load the exisitng urls from the database that need to be checked, and
    # check them against the sites.
    for url in needed_urls(db):
        # WOT
        if url['mywot_status'] == 'unknown':
            # mywot produces 'safe', 'warning', or 'untested'
            qual = run(['python', 'sbss/mywot.py', fmt_url(url['url'])]).strip()

            if qual == 'safe' or qual == 'warning' or qual == 'wtf':
                q = """UPDATE rate SET `mywot_status` = %s, `mywot` = %s
                       WHERE `id` = %s"""
                db.cursor().execute(q,
                    (qual, datetime.datetime.now(), url['id']))
                db.commit()

        # Norton
        if url['norton_status'] == 'unknown':
           # norton produces 'safe', 'warning', or 'untested'
           qual = run(['python', 'sbss/norton.py', fmt_url(url['url'])]).strip()

           if qual == 'safe' or qual == 'warning' or qual == 'wtf':
               q = """UPDATE rate SET `norton_status` = %s, `norton` = %s
                      WHERE `id` = %s"""
               db.cursor().execute(q,
                   (qual, datetime.datetime.now(), url['id']))
               db.commit()

        # SafeWeb
        if url['safeweb_status'] == 'unknown':
            # norton produces 'safe' or 'warning'
            qual = run(['python', 'sbss/safeweb.py', fmt_url(url['url'])]).strip()

            # if the return value is safe it may or may not be
            if qual == 'warning' or qual == 'wtf':
                q = """UPDATE rate SET `safeweb_status` = %s, `safeweb` = %s
                       WHERE `id` = %s"""
                db.cursor().execute(q,
                    (qual, datetime.datetime.now(), url['id']))
                db.commit()

    print "Finished at %s..." % datetime.datetime.now()

if __name__ == "__main__":
    main()
