import sys, os
import subprocess
import datetime
import hashlib
from couchdbkit import *
from couchdbkit.designer import push

def run(arguments):
    return subprocess.Popen(arguments,
        stdout=subprocess.PIPE).communicate()[0]

def fmt_url(url):
    if url.startswith("http"):
        return url
    else:
        return "http://" + url

class Url(Document):
    url = StringProperty()
    mywot = StringProperty()
    norton = StringProperty()
    safeweb = StringProperty()
    siteadvisor = StringProperty()
    date = DateTimeProperty()

def idify(url):
    m = hashlib.sha512()
    m.update(url)
    return m.hexdigest()

def add_url(url_in):
    try:
        u = Url(
            url = url_in,
            mywot = 'unknown',
            norton = 'unknown',
            safeweb = 'unknown',
            siteadvisor = 'unknown',
            date=datetime.datetime.utcnow()
        )
        u._id = idify(url_in)
        u.save()
    except exceptions.ResourceConflict:
        pass

def main():
    print "Starting at %s..." % datetime.datetime.now()

    database = sys.argv[1]
    command = sys.argv[2]

    # setup the database
    server = Server()
    db = server.get_or_create_db(database)
    Url.set_db(db)

    if command == 'init':
        # load the views
        push('_design/urls', db)

    # check what we're doing
    elif command == 'load':
        # deposit the urls into the database
        print "Adding new urls..."
        f = open(sys.argv[3])

        print "Loading all urls..."
        # load the exisitng urls from the database that need to be checked, and
        # check them against the sites.
        for url in f:
            try:
                add_url(url.strip())
            except:
                pass

        for url in Url.view('urls/unknown'):
            print "Checking %s..." % url.url
            # MyWOT
            if url.mywot == "unknown":
                # mywot produces 'safe', 'warning', or 'untested'
                qual = run(['python', 'sbss/mywot.py', fmt_url(url.url)]).strip()

                if qual == 'safe' or qual == 'warning' or qual == 'wtf':
                    url.mywot = qual
                    url.save()

            # Norton
            if url.norton == 'unknown':
                # norton produces 'safe', 'warning', or 'untested'
                qual = run(['python', 'sbss/norton.py', fmt_url(url['url'])]).strip()

                if qual == 'safe' or qual == 'warning' or qual == 'wtf':
                    url.norton = qual
                    url.save()
            # SafeWeb
            if url.safeweb == 'unknown':
                # norton produces 'safe' or 'warning'
                qual = run(['python', 'sbss/safeweb.py', fmt_url(url['url'])]).strip()

                # if the return value is safe it may or may not be
                if qual == 'safe' or qual == 'warning' or qual == 'wtf':
                    url.safeweb = qual
                    url.save()

            # Site Advisor
            if url.siteadvisor == 'unknown':
                qual = run(['python', 'sbss/siteadvisor.py', fmt_url(url['url'])]).strip()

                # if the return value is safe it may or may not be
                if qual == 'safe' or qual == 'warning' or qual == 'wtf':
                    url.siteadvisor = qual
                    url.save()

    elif command == 'results':
        urls = Url.view('urls/all')
        results = {
            'count': len(urls),

            'mywot_unknown': 0,
            'mywot_wtf': 0,
            'mywot_known': 0,

            'norton_unknown': 0,
            'norton_wtf': 0,
            'norton_known': 0,

            'safeweb_unknown': 0,
            'safeweb_wtf': 0,
            'safeweb_known': 0,

            'siteadvisor_unknown': 0,
            'siteadvisor_wtf': 0,
            'siteadvisor_known': 0,
        }
        for url in urls:
            if url.mywot == 'unknown':
                results['mywot_unknown'] += 1
            elif url.mywot == 'wtf':
                results['mywot_wtf'] += 1
            else:
                results['mywot_known'] += 1

            if url.norton == 'unknown':
                results['norton_unknown'] += 1
            elif url.norton == 'wtf':
                results['norton_wtf'] += 1
            else:
                results['norton_known'] += 1

            if url.safeweb == 'unknown':
                results['safeweb_unknown'] += 1
            elif url.safeweb == 'wtf':
                results['safeweb_wtf'] += 1
            else:
                results['safeweb_known'] += 1

            if url.siteadvisor == 'unknown':
                results['siteadvisor_unknown'] += 1
            elif url.siteadvisor == 'wtf':
                results['siteadvisor_wtf'] += 1
            else:
                results['siteadvisor_known'] += 1

        for x in results.iteritems():
            print "%s,%s" % x

    elif command == 'warning_sets':
        urls = Url.view('urls/all')
        mywot = []
        norton = []
        safeweb = []
        siteadvisor = []
        for url in urls:
            if url.mywot == 'warning':
                mywot.append(url.url)

            if url.norton == 'warning':
                norton.append(url.url)

            if url.safeweb == 'warning':
                safeweb.append(url.url)

            if url.siteadvisor == 'warning':
                siteadvisor.append(url.url)

        print "mywot = %s" % repr(mywot)
        print "norton = %s" % repr(norton)
        print "safeweb = %s" % repr(safeweb)
        print "siteadvisor = %s" % repr(siteadvisor)

    print "Finished at %s..." % datetime.datetime.now()


if __name__ == "__main__":
    main()
