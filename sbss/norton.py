#!/usr/bin/python

import sys
import os
from lxml import etree
import urllib

import utils

URL = "http://ratings-wrs.symantec.com/rating?url=%s"

def check_page(page):
    tree = etree.fromstring(page)

    try:
        try:
            rating = tree.find('site').attrib['r']
        except KeyError:
            return utils.UrlCats.untested

        if rating == 'w' or rating == 'b':
            return utils.UrlCats.warning
        elif rating == 'g' or rating == 'r':
            return utils.UrlCats.safe
        elif rating == 'u':
            return utils.UrlCats.untested
        else:
            return utils.UrlCats.error_checking
    except:
        return utils.UrlCats.error_checking

def usage():
    sys.stderr.write("USAGE: python norton.py URL\n" +\
        "\t -f: Follow URL redirects before checking.")
    sys.exit(1)

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        usage()

    try:
        if sys.argv[1] == '-f':
            url = utils.resolve_http_redirect(sys.argv[2])
        else:
            url = sys.argv[1]

        page = utils.load_url(URL % urllib.quote_plus(url))
        result = check_page(page.read())

        print result
    except ValueError:
        usage()
    except:
        print utils.UrlCats.error_checking

if __name__ == "__main__":
    main()
