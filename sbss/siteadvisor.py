#!/usr/bin/python

import sys
import os
import urllib
from BeautifulSoup import BeautifulSoup
import utils

URL = "http://www.siteadvisor.com/sites/%s"

def check_page(page):
    soup = BeautifulSoup(page)

    try:
        rating = soup.find('div', id='siteVerdict').find('img')['src'].lower()
        if 'yellow' in rating or 'red' in rating:
            return utils.UrlCats.warning
        elif 'green' in rating:
            return utils.UrlCats.safe
        elif 'grey' in rating:
            return utils.UrlCats.untested
        else:
            return utils.UrlCats.error_checking
    except:
        return utils.UrlCats.error_checking

def usage():
    sys.stderr.write("USAGE: python norton.py [-f] URL\n" +\
        "\t -f: Follow URL redirects before checking.")
    sys.exit(1)

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        usage()

    try:
        if sys.argv[1] == '-f':
            url = utils.resolve_http_redirect(sys.argv[2])
        else:
            url = sys.argv[1]

        page = utils.load_url(URL % url)

        result = check_page(page.read())

        print result
    except ValueError:
        usage()
    except:
        print utils.UrlCats.error_checking

if __name__ == "__main__":
    main()
