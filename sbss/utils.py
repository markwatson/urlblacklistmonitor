import mechanize
from lxml import etree
import lxml.html.soupparser
import lxml.html
import urlparse
import httplib

# Recursively follow redirects until there isn't a location header
def resolve_http_redirect(url, depth=0):
    if depth > 10:
        raise Exception("Redirected "+depth+" times, giving up.")
    o = urlparse.urlparse(url,allow_fragments=True)
    conn = httplib.HTTPConnection(o.netloc)
    path = o.path
    if o.query:
        path +='?'+o.query
    conn.request("HEAD", path)
    res = conn.getresponse()
    headers = dict(res.getheaders())
    if headers.has_key('location') and headers['location'] != url:
        return resolve_http_redirect(headers['location'], depth+1)
    else:
        return url

def load_url(url):
    # we're using mech so we can do cool stuff in the future.
    response = mechanize.urlopen(url)
    if response:
        return response

def get_tree(string):
    try:
        root = etree.fromstring(string)
        ignore = tostring(root, encoding=unicode)
    except:
        root = lxml.html.soupparser.fromstring(string)

    return root

class UrlCats(object):
    # the site is malicious
    warning = 'warning'

    # the site is unknown by the service
    untested = 'untested'

    # The site is tested and safe
    safe = 'safe'

    # We don't know about the site
    unknown = 'unknown'

    # there was an checking the URL
    error_checking = 'wtf'
