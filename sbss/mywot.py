#!/usr/bin/python

import sys
import os
from lxml import etree
import urllib

import utils

URL = 'http://api.mywot.com/0.4/public_query2?url=%s'

TRUSTWORTHINESS    = 0
VENDOR_RELIABILITY = 1
PRIVACY            = 2
CHILD_SAFETY       = 3

def check_page(page):
    tree = etree.fromstring(page)
    ratings = tree.findall('application')
    if not ratings:
        return utils.UrlCats.untested

    # take the average of all the types that are not child_safety
    ratings = [int(x.attrib['r']) for x in ratings
               if x.attrib['name'] != CHILD_SAFETY and x.attrib['c'] >= 23]
    av_rating = sum(ratings) / len(ratings)
    if av_rating >= 50:
        return utils.UrlCats.safe
    else:
        return utils.UrlCats.warning

def usage():
    sys.stderr.write("USAGE: python mywot.py URL\n" +\
        "\t -f: Follow URL redirects before checking.")
    sys.exit(1)

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        usage()

    try:
        if sys.argv[1] == '-f':
            url = utils.resolve_http_redirect(sys.argv[2])
        else:
            url = sys.argv[1]

        page = utils.load_url(URL % urllib.quote_plus(url))
        result = check_page(page.read())
        print result
    except ValueError:
        usage()
    except:
        print utils.UrlCats.error_checking

if __name__ == "__main__":
    main()
