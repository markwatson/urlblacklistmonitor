#!/usr/bin/python

import sys
import os
import urllib2
import urllib

import utils

# API key is ABQIAAAAuxuEIz3ElU8d4u3-uEyUtRRnSSqvExOTLYKg4u9sax6UQjU7Ww
URL = "https://sb-ssl.google.com/safebrowsing/api/lookup?client=demo-app&apikey=ABQIAAAAuxuEIz3ElU8d4u3-uEyUtRRnSSqvExOTLYKg4u9sax6UQjU7Ww&appver=1.5.2&pver=3.0&url=%s"

def check_page(url):
    req = urllib2.Request(URL % urllib.quote_plus(url))
    response = urllib2.urlopen(req)
    # the safeweb service lists the urls as suspicious or benign,
    if response.code == 200:
        return utils.UrlCats.warning
    elif response.code == 204:
        return utils.UrlCats.safe
    else:
        return utils.UrlCats.error_checking

def usage():
    sys.stderr.write("USAGE: python safeweb.py URL\n" +\
        "\t -f: Follow URL redirects before checking.")
    sys.exit(1)

def main():
    if len(sys.argv) != 2 and len(sys.argv) != 3:
        usage()

    if sys.argv[1] == '-f':
        url = utils.resolve_http_redirect(sys.argv[2])
    else:
        url = sys.argv[1]

    print check_page(url)

if __name__ == "__main__":
    main()
