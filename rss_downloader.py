#!/usr/bin/python

import sys, os
import lxml.html.soupparser
import lxml.html
from lxml.etree import tostring
from lxml import etree
import urllib2
from StringIO import StringIO
import datetime
import hashlib
import re

def md5(text):
    """ Provides the hexdigest of an md5 of the given text """
    m = hashlib.md5()
    m.update(text)
    return m.hexdigest()

class Rss(object):
    class RssItem(object):
        def __init__(self, title, link, description):
            self.title = title
            self.link = link
            self.description = description

        def __repr__(self):
            return "RssItem(title='%s', link='%s', description='%s')" % \
                (self.title, self.link, self.description)

    def __init__(self, resource):
        """ Start a new RSS object based on the indicated resource. """
        if os.path.isfile(resource):
            self._open_file(resource)
        else:
            try:
                self._open_url(resource)
            except:
                raise Exception("Can't access resource.")

        self._open_tree()

    def _open_file(self, filename):
        self.handle = open(filename)
        self.type = "file"

    def _open_url(self, url):
        self.handle = urllib2.urlopen(url)
        self.type = "url"

    def _open_tree(self):
        self.raw_string = ''.join(self.handle.readlines())

        # try decoding with two parsers ;)
        try:
            parser = etree.XMLParser(strip_cdata=False)
            self.root = etree.fromstring(self.raw_string, parser)
            ignore = tostring(self.root, encoding=unicode)
        except:
            self.root = lxml.html.soupparser.fromstring(self.raw_string)
            self.root = self.root.find('rss')

    def _get_main_attr(self, name):
        channel = self.root.find('channel')
        if channel is not None:
            channel_name = channel.find(name)
            if channel_name is not None:
                return channel_name.text

    @property
    def title(self):
        return self._get_main_attr('title')

    @property
    def link(self):
        return self._get_main_attr('link')

    @property
    def description(self):
        return self._get_main_attr('description')

    @property
    def language(self):
        return self._get_main_attr('language')

    @property
    def copyright(self):
        return self._get_main_attr('copyright')

    @property
    def items(self):
        items = self.root.find('channel').findall('item')
        if items is not None:
            for item in items:
                yield self.RssItem(
                    item.find('title').text,
                    item.find('link').text,
                    item.find('description').text
                )

FEEDS = {
    'malc0de.com': 'http://malc0de.com/rss/',
    'hosts-file.net': 'http://hosts-file.net/rss.asp',
    'malwaredomainlist.com': 'http://www.malwaredomainlist.com/hostslist/mdl.xml',
    'malwareblacklist.com': 'http://www.malwareblacklist.com/mbl.xml',
}

# FEEDS = {
#     'malc0de.com': 'sample_malc0de.com.rss',
#     'hosts-file.net': 'sample_hosts-file.net.rss',
#     'malwaredomainlist.com': 'sample_malwaredomainlist.com.rss',
#     'malwareblacklist.com': 'sample_malwareblacklist.com.rss',
# }

def usage():
    print "python rss_donwloader.py SOURCE"
    print '\tPossible sources: ' + ', '.join(FEEDS.keys())
    sys.exit(1)

def output_header(name, url, copyright, desc):
    sys.stdout.write("SOURCE,1.0,%s,%s,%s,%s\n" %
                     (name,url,copyright,desc))

def output_item(malicious_url, cat, info_url, source_date, date_found, ip, md5, desc):
    sys.stdout.write("URL,1.0,%s,%s,%s,%s,%s,%s,%s,%s\n" %
                     (malicious_url, cat, info_url, source_date, date_found, ip, md5, desc))

def to_ts(dt):
    return dt.strftime('%Y%m%d%H%M%S')

def current_tstamp():
    return to_ts(datetime.datetime.now())

def main():
    # get command line argument
    if len(sys.argv) != 2:
        usage()
    feed_type = sys.argv[1]
    if feed_type not in FEEDS:
        usage()

    # get the feed
    r = Rss(FEEDS[feed_type])

    # do feed specific formatting and outputting
    # these sections are all simular, but different...
    # given more time it would be good to refactor these into functions
    if feed_type == 'malc0de.com':
        output_header(r.title, FEEDS[feed_type], r.copyright, r.description)

        for x in r.items:
            try:
                # parse description
                key_vals = map(str.strip, x.description.split(','))
                key_vals = dict(map(str.strip, x.split(':')[:2])
                                for x in key_vals)

                # output the item
                output_item(
                    malicious_url = key_vals['URL'],
                    cat = 'VirusEXE',
                    info_url = x.link,
                    source_date = None,
                    date_found = current_tstamp(),
                    ip = key_vals.get('IP Address', None),
                    md5 = key_vals.get('MD5', None),
                    desc = x.description.replace(',', ';')
                )
            except:
                sys.stderr.write("Error: couldn't output item...\n")
    elif feed_type == 'malwaredomainlist.com':
        output_header(r.title, FEEDS[feed_type], r.copyright, r.description)

        for x in r.items:
            try:
                # parse description
                key_vals = map(str.strip, x.description.split(','))
                key_vals = dict(map(str.strip, x.split(':')[:2])
                                for x in key_vals)

                host = key_vals.get('Host')
                if not host:
                    host = key_vals.get('IP address')

                # output the item
                output_item(
                    malicious_url = host,
                    cat = 'Unknown',
                    info_url = x.link,
                    source_date = None,
                    date_found = current_tstamp(),
                    ip = key_vals.get('IP address'),
                    md5 = md5(host),
                    desc = x.description.replace(',', ';')
                )
            except:
                sys.stderr.write("Error: couldn't output item...\n")
    elif feed_type == 'hosts-file.net':
        output_header(r.title, FEEDS[feed_type], r.copyright, r.description)

        for x in r.items:
            try:
                # parse description
                key_vals_temp = map(str.strip, x.description.split('<br>'))
                key_vals = {}
                for k in key_vals_temp:
                    t = k.split(':')
                    key = t[0].strip()
                    val = ':'.join(t[1:]).strip()
                    key_vals[key] = val

                d = to_ts(datetime.datetime.strptime(
                          key_vals['Added'], '%m/%d/%Y %I:%M:%S %p'))

                # output the item
                output_item(
                    malicious_url = key_vals['Website'],
                    cat = key_vals.get('Classification', 'Unknown'),
                    info_url = x.link,
                    source_date = d,
                    date_found = current_tstamp(),
                    ip = key_vals.get('IP'),
                    md5 = md5(key_vals['Website']),
                    desc = x.description.replace('<br>', '; ')
                )
            except:
                sys.stderr.write("Error: couldn't output item...\n")
    elif feed_type == 'malwareblacklist.com':
        output_header(r.title, FEEDS[feed_type], r.copyright, r.description)

        re_title = re.compile('([^\(]+)\(([^\)]+)\)')
        for x in r.items:
            try:
                # parse description
                key_vals = map(str.strip, str(x.description).split(','))
                key_vals = dict(map(str.strip, x.split(':')[:2])
                                for x in key_vals)

                # parse out the date
                d = None
                host = None
                try:
                    m = re_title.match(x.title)
                    if m:
                        d = to_ts(datetime.datetime.strptime(
                              m.group(2), '%b  %d %Y %I:%M:%S:%f%p'))
                except:
                    pass

                # output the item
                output_item(
                    malicious_url = key_vals['Host'],
                    cat = 'Unknown',
                    info_url = x.link,
                    source_date = d,
                    date_found = current_tstamp(),
                    ip = key_vals.get('IP address'),
                    md5 = md5(key_vals['Host']),
                    desc = x.description.replace(',', ';')
                )
            except:
                sys.stderr.write("Error: couldn't output item...\n")

if __name__ == "__main__":
    main()
