function(doc) {
    if (doc.doc_type == "Url" &&
        (doc.mywot == "unknown" ||
         doc.norton == "unknown" ||
         doc.safeweb == "unknown" ||
         doc.siteadvisor == "unknown") )
        emit(doc._id, doc);
}
